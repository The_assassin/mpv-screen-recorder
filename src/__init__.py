# =====================================================================
# This file is part of mpv-screen-recorder.
#
# Copyright (C) 2019 - 2021 Zdravko Mitov <mitovz@mail.fr>
#
# mpv-screen-recorder is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# =====================================================================
REPO = "https://gitlab.com/The_assassin/mpv-screen-recorder"
CR_YRS = "Copyright © 2019 - 2021"
TITLE = "Screen Recorder"
AUTHOR = "Zdravko Mitov"
MAIL = "<mitovz@mail.fr>"
VERSION = "0.3.1"
APP_ID = "org.gtk.MpvScreenRecorder"
APP_NAME = "mpv-screen-recorder"
APP_ICON = "user-desktop"
REC_ICON = "media-record"
PAUSE_ICON = "media-playback-pause"
CHCK_LOG = "/tmp/mpv_scr_rec_fail.log"
