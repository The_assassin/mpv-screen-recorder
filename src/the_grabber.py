# =====================================================================
# This file is based of “screenshot-area-selection.c“ in “gnome-screenshot“ software.
# It is a part of mpv-screen-recorder.
#
# Copyright (C) 2019 - 2021 Zdravko Mitov <mitovz@mail.fr>
#
# mpv-screen-recorder is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# =====================================================================

from gi.repository import Gdk
from gi.repository import Gtk
import cairo


class AreaSelector:

    def __init__(self):

        self.__rect = Gdk.Rectangle()
        self.__rect.x = 0
        self.__rect.y = 0
        self.__rect.width = 0
        self.__rect.height = 0
        self.__button_pressed = False
        self.__aborted = False
        self.__window = self.__create_select_window()

        self.__window.connect("key-press-event", self.__select_area_key_press)
        self.__window.connect("button-press-event", self.__select_area_button_press)
        self.__window.connect("button-release-event", self.__select_area_button_release)
        self.__window.connect("motion-notify-event", self.__select_area_motion_notify)

        display = self.__window.get_display()
        cursor = Gdk.Cursor.new_for_display(display, Gdk.CursorType.CROSSHAIR)
        manager = Gdk.Display.get_device_manager(display)
        pointer = Gdk.DeviceManager.get_client_pointer(manager)
        keyboard = Gdk.Device.get_associated_device(pointer)

        res = Gdk.Device.grab(pointer, self.__window.get_window(),
                              Gdk.GrabOwnership.NONE, False,
                              Gdk.EventMask.POINTER_MOTION_MASK |
                              Gdk.EventMask.BUTTON_PRESS_MASK |
                              Gdk.EventMask.BUTTON_RELEASE_MASK,
                              cursor, Gdk.CURRENT_TIME)

        if not res == Gdk.GrabStatus.SUCCESS:
            Gdk.Cursor.unref

        res = Gdk.Device.grab(keyboard, self.__window.get_window(),
                              Gdk.GrabOwnership.NONE, False,
                              Gdk.EventMask.KEY_PRESS_MASK |
                              Gdk.EventMask.KEY_RELEASE_MASK,
                              None, Gdk.CURRENT_TIME)

        if not res == Gdk.GrabStatus.SUCCESS:
            Gdk.Device.ungrab(pointer, Gdk.CURRENT_TIME)
            Gdk.Cursor.unref

        Gtk.main()

        Gdk.Device.ungrab(pointer, Gdk.CURRENT_TIME)
        Gdk.Device.ungrab(keyboard, Gdk.CURRENT_TIME)

        self.__window.destroy()
        Gdk.Cursor.unref
        Gdk.flush()

    @property
    def rect(self):
        return self.__rect

    @property
    def aborted(self):
        return self.__aborted

    def __select_area_key_press(self, widget, event):
        if (event.keyval == Gdk.KEY_Escape):
            self.__rect.x = 0
            self.__rect.y = 0
            self.__rect.width = 0
            self.__aborted = True

            Gtk.main_quit()

        return True

    def __select_area_button_press(self, widget, event):
        if self.__button_pressed:
            return True

        self.__button_pressed = True
        self.__rect.x = event.x_root
        self.__rect.y = event.y_root

        return True

    def __select_area_button_release(self, widget, event):

        if not self.__button_pressed:
            return True

        self.__rect.width = abs(self.__rect.x - event.x_root)
        self.__rect.height = abs(self.__rect.y - event.y_root)
        self.__rect.x = min(self.__rect.x, event.x_root)
        self.__rect.y = min(self.__rect.y, event.y_root)

        if (self.__rect.width == 0 or self.__rect.height == 0):
            self.__aborted = True

        Gtk.main_quit()

    def __select_area_motion_notify(self, window, event):
        draw_rect = Gdk.Rectangle()

        if not self.__button_pressed:
            return True

        draw_rect.width = abs(self.__rect.x - event.x_root)
        draw_rect.height = abs(self.__rect.y - event.y_root)
        draw_rect.x = min(self.__rect.x, event.x_root)
        draw_rect.y = min(self.__rect.y, event.y_root)

        if (draw_rect.width <= 0 or draw_rect.height <= 0):
            window.move(-100, -100)
            window.resize(10, 10)
            return True

        window.move(draw_rect.x, draw_rect.y)
        window.resize(draw_rect.width, draw_rect.height)

        # We (ab)use app-paintable to indicate if we have an RGBA window
        if not Gtk.Widget.get_app_paintable(window):
            try:
                gdk_window = Gtk.Widget.get_window(window)
                if (draw_rect.width > 2 and draw_rect.height > 2):
                    region_rect = cairo.RectangleInt(0, 0,
                                                     draw_rect.width,
                                                     draw_rect.height)
                    region = cairo.Region(region_rect)
                    region_rect.x += 1
                    region_rect.y += 1
                    region_rect.width -= 2
                    region_rect.height -= 2
                    cairo.Region.subtract(region, region_rect)

                    gdk_window.shape_combine_region(region, 0, 0)

                    del region

                else:
                    gdk_window.shape_combine_region(None, 0, 0)
            except AttributeError:
                Gtk.Widget.set_app_paintable(window, True)

        return True

    def __select_window_draw(self, window, cr):
        style = window.get_style_context()

        if window.get_app_paintable():
            cr.set_operator(cairo.OPERATOR_SOURCE)
            cr.set_source_rgba(0.0, 0.0, 0.0, 0.0)
            cr.paint()

            frm_w = window.get_allocated_width()
            frm_h = window.get_allocated_height()

            Gtk.StyleContext.save(style)
            Gtk.StyleContext.add_class(style, Gtk.STYLE_CLASS_RUBBERBAND)

            Gtk.render_background(style, cr, 0, 0, frm_w, frm_h)

            Gtk.render_frame(style, cr, 0, 0, frm_w, frm_h)

            cr.set_font_size(round(frm_h / 6))
            cr.set_source_rgba(1.0, 1.0, 1.0, 0.8)

            (x_bear, y_bear, w_glph, h_glph, x_adv, y_adv) = cr.text_extents("101010101")

            cr.move_to(frm_w / 2 - w_glph / 2, frm_h / 2)
            cr.show_text("%d X %d" % (frm_w, frm_h))

            Gtk.StyleContext.restore(style)

        return True

    def __create_select_window(self):
        screen = Gdk.Screen.get_default()
        visual = Gdk.Screen.get_rgba_visual(screen)
        window = Gtk.Window.new(Gtk.WindowType.POPUP)
        if (Gdk.Screen.is_composited(screen) and visual):
            window.set_visual(visual)
            window.set_app_paintable(True)

        window.connect("draw", self.__select_window_draw)

        window.move(-100, -100)
        window.resize(10, 10)
        window.show()

        return window
