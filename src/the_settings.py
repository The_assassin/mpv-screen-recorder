# =====================================================================
# This file is part of mpv-screen-recorder
#
# Copyright (C) 2019 - 2021 Zdravko Mitov <mitovz@mail.fr>
#
# mpv-screen-recorder is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# =====================================================================
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import Gio
from gi.repository import GLib
from mpv_screen_recorder import *


class SettingsWindow(Gtk.Dialog):

    def __init__(self, win):
        Gtk.Dialog.__init__(self, title="Screen Recorder settings",
                            parent=win,
                            flags=Gtk.DialogFlags.DESTROY_WITH_PARENT)
        self.set_border_width(10)
        self.set_resizable(False)
        self.__settings = Gio.Settings(schema=APP_ID)
        self.__main = win
        self.set_tooltip_text("Restart is recomended after doing changes on this window")

        self.connect("focus-in-event", self.__on_update_changes_from_parrent)

        content_area = self.get_content_area()
        main_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        content_area.add(main_box)

        val = self.__settings.get_string("recordings-folder")
        tooltip = "Click to select another recordings folder."
        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        label = self.__on_create_label("Recordings folder")
        label.set_tooltip_text("Set a recordings folder")
        entry = Gtk.Entry()
        entry.set_text(val)
        entry.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, "folder-open")
        entry.set_icon_activatable(Gtk.EntryIconPosition.SECONDARY, True)
        entry.set_icon_tooltip_text(Gtk.EntryIconPosition.SECONDARY, tooltip)
        entry.connect("icon-press", self.__on_entry_icon_clicked)
        entry.connect("notify::text", self.__on_entry_text, "recordings-folder")
        hbox.pack_start(label, False, True, 2)
        hbox.pack_start(entry, True, True, 2)
        main_box.add(hbox)

        val = self.__settings.get_string("file-name")
        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=55)
        label = self.__on_create_label("File name")
        label.set_tooltip_text("Set just a name, time, date and extension will be add automatically.")
        entry = Gtk.Entry()
        entry.set_text(val)
        entry.connect("notify::text", self.__on_entry_text, "file-name")
        hbox.pack_start(label, False, True, 2)
        hbox.pack_start(entry, True, True, 2)
        main_box.add(hbox)

        val = self.__settings.get_string("custom-output-format")
        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=80)
        label = self.__on_create_label("Custom output format")
        label.set_tooltip_markup("A string specifying the custom output format. The “<u><i>Output format</i></u>“ option should be set to “<u><b>custom</b></u>“.\n<b>NOTE:</b> Make sure the proper format will be set, otherwise the record might fail! (<b>“see mpv --of=help“</b>)")
        entry = Gtk.Entry()
        entry.set_text(val)
        entry.connect("notify::text", self.__on_entry_text, "custom-output-format")
        hbox.pack_start(label, True, True, 2)
        hbox.pack_start(entry, False, True, 2)
        main_box.add(hbox)

        val = self.__settings.get_string("custom-extension")
        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=80)
        label = self.__on_create_label("Custom extension")
        label.set_tooltip_markup("A string specifying the custom file extension. <u>Depends of the custom format setting.</u>")
        entry = Gtk.Entry()
        entry.set_text(val)
        entry.connect("notify::text", self.__on_entry_text, "custom-extension")
        hbox.pack_start(label, True, True, 2)
        hbox.pack_start(entry, False, True, 2)
        main_box.add(hbox)

        index = self.__settings.get_enum("output-format")
        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=20)
        label = self.__on_create_label("Output format")
        codecs_list = Gtk.ListStore(int, str)
        codecs_list.append([0, "h264"])
        codecs_list.append([1, "hevc"])
        codecs_list.append([2, "m4v"])
        codecs_list.append([3, "matroska"])
        codecs_list.append([4, "mp4"])
        codecs_list.append([5, "mpeg2video"])
        codecs_list.append([6, "ogv"])
        codecs_list.append([7, "webm"])
        codecs_list.append([8, "custom"])
        codec_combo = Gtk.ComboBox.new_with_model(codecs_list)
        codec_combo.set_active(index)
        codec_combo.connect("changed", self.__on_codec_combo_changed)
        renderer_text = Gtk.CellRendererText()
        codec_combo.pack_start(renderer_text, True)
        codec_combo.add_attribute(renderer_text, "text", 1)
        hbox.pack_start(label, True, True, 2)
        hbox.pack_start(codec_combo, False, True, 2)
        main_box.add(hbox)

        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=20)
        label = self.__on_create_label("Grabbing frame rate")
        label.set_tooltip_text("Set the grabbing frame rate. Default value is 25.000")
        curr = self.__settings.get_double("frame-rate")
        adjust = Gtk.Adjustment.new(curr, 1.0, 60.0, 0.1, 1.0, 0)
        spin = Gtk.SpinButton.new(adjust, 1.0, 3)
        spin.connect("value-changed", self.__on_spin_value_changed_cb, "frame-rate")
        hbox.pack_start(label, True, True, 2)
        hbox.pack_start(spin, False, True, 2)
        main_box.add(hbox)

        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=20)
        label = self.__on_create_label("Seconds to delay before recording")
        curr = self.__settings.get_int("start-delay")
        adjust = Gtk.Adjustment.new(curr, 0, 99, 1, 1, 0)
        self.__delay_spin = Gtk.SpinButton.new(adjust, 1.0, 0)
        self.__delay_spin.connect("value-changed", self.__main._delay_spin_value_changed_cb)
        hbox.pack_start(label, True, True, 2)
        hbox.pack_start(self.__delay_spin, False, True, 2)
        main_box.add(hbox)

        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=20)
        label = self.__on_create_label("Pixels to the edge of region when follow the mouse")
        label.set_tooltip_text("When it is specified with 0, the grabbing region follows the mouse pointer and keeps the pointer at the center of region,otherwise, the region follows only when the mouse pointer\nreaches within PIXELS (greater than zero) to the edge of region.")
        cur_fm = self.__settings.get_int("follow-mouse")
        cur_vs = self.__settings.get_value("video-size")
        cur_up = int(cur_vs[1] * 0.4)
        self.__fm_adj = Gtk.Adjustment.new(cur_fm, 0, cur_up, 1, 10, 0)
        self.__fm_spin = Gtk.SpinButton.new(self.__fm_adj, 1.0, 0)
        self.__fm_spin.connect("value-changed", self.__main._pixels_spin_value_changed_cb)
        hbox.pack_start(label, True, True, 2)
        hbox.pack_start(self.__fm_spin, False, True, 2)
        main_box.add(hbox)

        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=20)
        label = self.__on_create_label("Region border thickness")
        label.set_tooltip_text("Set the region border thickness if “Show grabbed region“ is used.\nRange is 1 to 128 and default is 3.")
        cur_rb = self.__settings.get_int("region-border")
        adjust = Gtk.Adjustment.new(cur_rb, 1, 128, 1, 4, 0)
        spin = Gtk.SpinButton.new(adjust, 1.0, 0)
        spin.connect("value-changed", self.__on_spin_value_changed_cb, "region-border")
        hbox.pack_start(label, True, True, 2)
        hbox.pack_start(spin, False, True, 2)
        main_box.add(hbox)

        init_wt = 160
        init_hg = 128
        scr_hgh = Gdk.Display.get_default().get_default_screen().get_height()
        ars_lst = [(init_wt * i, init_hg * i) for i in range(2, 25)
                   if (init_hg * i) < scr_hgh]
        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=20)
        label = self.__on_create_label("Video frame size when follow the mouse")
        label.set_tooltip_text("Set the video frame size when follow the mouse. Default value is 320 X 256 (AR 5:4).")
        res_list = Gtk.ListStore(str, int, int)
        for i, tpl in enumerate(ars_lst):
            res_list.append(["%d X %d" % (tpl[0], tpl[1]), tpl[0], tpl[1]])
        cur_vs = self.__settings.get_value("video-size")
        res_combo = Gtk.ComboBox.new_with_model(res_list)
        res_combo.set_active(ars_lst.index((cur_vs[0], cur_vs[1])))
        res_combo.connect("changed", self.__on_res_combo_changed)
        renderer_text = Gtk.CellRendererText()
        res_combo.pack_start(renderer_text, True)
        res_combo.add_attribute(renderer_text, "text", 0)
        hbox.pack_start(label, True, True, 2)
        hbox.pack_start(res_combo, False, True, 2)
        main_box.add(hbox)

        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=20)
        label = self.__on_create_label("Draw the mouse pointer")
        label.set_tooltip_text("Specify whether to draw the mouse pointer. Default value is “ON“")
        swtch_state = self.__settings.get_enum("draw-mouse") == 1
        self.__dm_switch = Gtk.Switch(active=swtch_state)
        self.__dm_switch.connect("notify::active", self.__main._on_incl_pointer_toggled_cb)
        hbox.pack_start(label, True, True, 2)
        hbox.pack_start(self.__dm_switch, False, True, 2)
        main_box.add(hbox)

        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=20)
        label = self.__on_create_label("Show grabbed region on screen")
        label.set_tooltip_text("With this option, it is easy to know what is being grabbed if only a portion of the screen is grabbed.")
        swtch_state = self.__settings.get_enum("show-region") == 1
        switch = Gtk.Switch(active=swtch_state)
        switch.connect("notify::active", self.__on_switch_toggled_cb, "show-region")
        hbox.pack_start(label, True, True, 2)
        hbox.pack_start(switch, False, True, 2)
        main_box.add(hbox)

        self.show_all()

    def __on_codec_combo_changed(self, combo):

        tree_iter = combo.get_active_iter()
        if tree_iter is not None:
            model = combo.get_model()
            codec = model[tree_iter][0]
            self.__settings.set_enum("output-format", codec)

    def __on_create_label(self, text):
        label = Gtk.Label.new()
        label.set_markup("<span weight=\"ultrabold\">%s</span>" % text)
        label.set_xalign(0)
        return label

    def __on_switch_toggled_cb(self, button, data, key):
        if button.get_active():
            self.__settings.set_enum(key, 1)
        else:
            self.__settings.set_enum(key, 0)

    def __on_entry_icon_clicked(self, entry, pos, button):
        dialog = Gtk.FileChooserDialog(title="Select Folder",
                                       parent=self,
                                       flags=Gtk.DialogFlags.DESTROY_WITH_PARENT,
                                       action=Gtk.FileChooserAction.SELECT_FOLDER,
                                       buttons=(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                                Gtk.STOCK_OPEN, Gtk.ResponseType.OK))
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            folder_path = dialog.get_filename()
            dialog.destroy()
            entry.set_text(folder_path)
        dialog.destroy()

    def __on_spin_value_changed_cb(self, button, key):
        if key == "frame-rate":
            val = float(button.get_value())
            self.__settings.set_double("frame-rate", val)
        else:
            self.__settings.set_int(key, button.get_value_as_int())

    def __on_res_combo_changed(self, combo):
        tree_iter = combo.get_active_iter()
        if tree_iter is not None:
            model = combo.get_model()
            res_w = model[tree_iter][1]
            res_h = model[tree_iter][2]
            self.__settings.set_value("video-size", GLib.Variant("(ii)", (res_w, res_h)))
            self.__settings.set_int("follow-mouse", 0)
            self.__fm_adj.props.upper = int(res_h * 0.4)
            self.__main.fm_adj.props.upper = int(res_h * 0.4)

    def __on_entry_text(self, entry, gprm, key):
        self.__settings.set_string(key, entry.get_text())

    def __on_update_changes_from_parrent(self, win, event):
        self.__delay_spin.set_value(self.__settings.get_int("start-delay"))
        self.__fm_spin.set_value(self.__settings.get_int("follow-mouse"))
        dm_val = self.__settings.get_enum("draw-mouse")
        if dm_val > 0:
            self.__dm_switch.set_active(True)
        else:
            self.__dm_switch.set_active(False)
